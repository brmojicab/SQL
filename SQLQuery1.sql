create database ronaldotest;
use ronaldotest;
/*drop database ronaldotest;*/
create table Persona (
	PersonaId int,
	Nombre varchar(255),
	Apellido varchar(255)
);

/*delte table or delete data*/
drop table Persona;
truncate table Persona;

/*add column table*/
alter table Persona add Ciudad varchar(255);

/*drop column*/
alter table Persona drop column Ciudad;

/*modify column*/
ALTER TABLE table_name
ALTER COLUMN column_name datatype;

/*constraints*/
/*
	NOT NULL - Ensures that a column cannot have a NULL value
	UNIQUE - Ensures that all values in a column are different
	PRIMARY KEY - A combination of a NOT NULL and UNIQUE. Uniquely identifies each row in a table
	FOREIGN KEY - Uniquely identifies a row/record in another table
	CHECK - Ensures that all values in a column satisfies a specific condition
	DEFAULT - Sets a default value for a column when no value is specified
	INDEX - Use to create and retrieve data from the database very quickl
*/

/*Primary key*/
CREATE TABLE Persons (
    ID int NOT NULL PRIMARY KEY,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int
);

CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    CONSTRAINT PK_Person PRIMARY KEY (ID,LastName)
);

ALTER TABLE Persons
ADD PRIMARY KEY (ID);

/*Foreign key*/

CREATE TABLE Orders (
    OrderID int NOT NULL PRIMARY KEY,
    OrderNumber int NOT NULL,
    PersonID int FOREIGN KEY REFERENCES Persons(PersonID)
);

CREATE TABLE Orders (
    OrderID int NOT NULL,
    OrderNumber int NOT NULL,
    PersonID int,
    PRIMARY KEY (OrderID),
    CONSTRAINT FK_PersonOrder FOREIGN KEY (PersonID)
    REFERENCES Persons(PersonID)
);

ALTER TABLE Orders
ADD FOREIGN KEY (PersonID) REFERENCES Persons(PersonID);

/*check*/

CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int CHECK (Age>=18)
);

/*default value*/
CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    City varchar(255) DEFAULT 'Sandnes'
);

/*auto increment*/

CREATE TABLE Persons (
    ID int IDENTITY(1,1) PRIMARY KEY,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int
);
INSERT INTO Persons (FirstName,LastName)
VALUES ('Lars','Monsen');