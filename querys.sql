/*select*/
SELECT column1, column2
FROM table_name;

SELECT * FROM table_name;

/*conditional*/

SELECT column1, column2
FROM table_name
WHERE condition1 AND condition2 AND condition3;

SELECT column1, column2
FROM table_name
WHERE condition1 OR condition2 OR condition3;

SELECT column1, column2
FROM table_name
WHERE NOT condition;

/*order by*/
SELECT * FROM Customers
ORDER BY Country DESc;

/*Inner join*/
SELECT column_name(s)
FROM table1
INNER JOIN table2 ON table1.column_name = table2.column_name;

/*group by*/
SELECT COUNT(CustomerID), Country
FROM Customers
GROUP BY Country; /*Ordena por ciudad y cuenta cuantos customers hay por ciudad*/